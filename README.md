# Project 4: Brevet time calculator with Ajax

This is a reimplimation of the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

Can calculate brevits up to 1000km and with a maxium of 20 control points.

This calculator does implement the french algorithm for control points less then 60km

# Use

To use this calculator first select your brevet distance, start date, and start time. 

Although it will update the controls open close times if you want to change any of these values later.

After this you are free to enter your control distances in either km or miles, however the total distance is always in km.


# Notes

If you enter a control point it must be three things:
	
	-A non negative number(floats are fine)
	-No longer than 20% the total length of the brevit
	-Bigger than the previous control point
	

# Developers 


There is a test file include incase you want to modify the code and test the new implementation

To run the test file you must have an open docker container and be in the brevet folder then call:

docker exec -it <container_id> nosetests

If you want to increase the brevet length there is a table that handles cases when a control point is over the brevet length but not more than 20%. This would be a good place to start.

# How times are calculated 

If interested in how times are calculated.
See this link: https://rusa.org/pages/acp-brevet-control-times-calculator

# Author

Evan Kender

ekender@uoregon.edu


base code author: Michal Young

# Requirements

arrow,
Flask,
nose,
pep8,
and utopep8.





